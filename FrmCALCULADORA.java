
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class FrmCALCULADORA extends javax.swing.JFrame {
    
    String memoria1;
    String signo;
    String memoria2;
    
    DefaultListModel lista;
    
    public FrmCALCULADORA() {
        
        initComponents();
        
        lista = new DefaultListModel();
        Historial.setModel(lista);
        
        this.setLocationRelativeTo(this); //centrar Programa en la pantalla. //
    }
    
    public void agregarH(){
        
        String valor = txtpantalla.getText();
        lista.addElement(valor);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        Botón9 = new javax.swing.JButton();
        Botón6 = new javax.swing.JButton();
        BotónResta = new javax.swing.JButton();
        BotónDivisión = new javax.swing.JButton();
        BotónMultiplicar = new javax.swing.JButton();
        BotónSuma = new javax.swing.JButton();
        BotónDEL = new javax.swing.JButton();
        BotónResultado = new javax.swing.JButton();
        BotónAC = new javax.swing.JButton();
        BotónPI = new javax.swing.JButton();
        BotónEuler = new javax.swing.JButton();
        BotónRaízEnécima = new javax.swing.JButton();
        txtpantalla = new javax.swing.JTextField();
        BotónRaízCuadrada = new javax.swing.JButton();
        Botón1 = new javax.swing.JButton();
        BotónExponente = new javax.swing.JButton();
        Botón4 = new javax.swing.JButton();
        BotónParéntesisIzquierdo = new javax.swing.JButton();
        Botón7 = new javax.swing.JButton();
        BotónParéntesisDerecho = new javax.swing.JButton();
        Botón0 = new javax.swing.JButton();
        BotónLOG = new javax.swing.JButton();
        Botón2 = new javax.swing.JButton();
        Botón8 = new javax.swing.JButton();
        Botón5 = new javax.swing.JButton();
        Botón3 = new javax.swing.JButton();
        BotónPunto = new javax.swing.JButton();
        BotónDivisión1 = new javax.swing.JButton();
        BotónPorcentaje_de_num = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        Historial = new javax.swing.JList();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();

        jToolBar1.setRollover(true);

        jList1.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(jList1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Botón9.setBackground(new java.awt.Color(51, 51, 51));
        Botón9.setForeground(new java.awt.Color(255, 255, 255));
        Botón9.setText("9");
        Botón9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón9ActionPerformed(evt);
            }
        });

        Botón6.setBackground(new java.awt.Color(51, 51, 51));
        Botón6.setForeground(new java.awt.Color(255, 255, 255));
        Botón6.setText("6");
        Botón6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón6ActionPerformed(evt);
            }
        });

        BotónResta.setBackground(new java.awt.Color(51, 51, 51));
        BotónResta.setForeground(new java.awt.Color(255, 255, 255));
        BotónResta.setText("-");
        BotónResta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónRestaActionPerformed(evt);
            }
        });

        BotónDivisión.setBackground(new java.awt.Color(51, 51, 51));
        BotónDivisión.setForeground(new java.awt.Color(255, 255, 255));
        BotónDivisión.setText("/");
        BotónDivisión.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónDivisiónActionPerformed(evt);
            }
        });

        BotónMultiplicar.setBackground(new java.awt.Color(51, 51, 51));
        BotónMultiplicar.setForeground(new java.awt.Color(255, 255, 255));
        BotónMultiplicar.setText("*");
        BotónMultiplicar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónMultiplicarActionPerformed(evt);
            }
        });

        BotónSuma.setBackground(new java.awt.Color(51, 51, 51));
        BotónSuma.setForeground(new java.awt.Color(255, 255, 255));
        BotónSuma.setText("+");
        BotónSuma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónSumaActionPerformed(evt);
            }
        });

        BotónDEL.setBackground(new java.awt.Color(153, 51, 0));
        BotónDEL.setForeground(new java.awt.Color(255, 255, 255));
        BotónDEL.setText("Del");
        BotónDEL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónDELActionPerformed(evt);
            }
        });

        BotónResultado.setBackground(new java.awt.Color(51, 51, 51));
        BotónResultado.setForeground(new java.awt.Color(255, 255, 255));
        BotónResultado.setText("=");
        BotónResultado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónResultadoActionPerformed(evt);
            }
        });

        BotónAC.setBackground(new java.awt.Color(153, 51, 0));
        BotónAC.setForeground(new java.awt.Color(255, 255, 255));
        BotónAC.setText("AC");
        BotónAC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónACActionPerformed(evt);
            }
        });

        BotónPI.setBackground(new java.awt.Color(51, 51, 51));
        BotónPI.setForeground(new java.awt.Color(255, 255, 255));
        BotónPI.setText("PI");
        BotónPI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónPIActionPerformed(evt);
            }
        });

        BotónEuler.setBackground(new java.awt.Color(51, 51, 51));
        BotónEuler.setForeground(new java.awt.Color(255, 255, 255));
        BotónEuler.setText("e");
        BotónEuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónEulerActionPerformed(evt);
            }
        });

        BotónRaízEnécima.setBackground(new java.awt.Color(51, 51, 51));
        BotónRaízEnécima.setForeground(new java.awt.Color(255, 255, 255));
        BotónRaízEnécima.setText("x√");
        BotónRaízEnécima.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónRaízEnécimaActionPerformed(evt);
            }
        });

        txtpantalla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpantallaActionPerformed(evt);
            }
        });

        BotónRaízCuadrada.setBackground(new java.awt.Color(51, 51, 51));
        BotónRaízCuadrada.setForeground(new java.awt.Color(255, 255, 255));
        BotónRaízCuadrada.setText("√");
        BotónRaízCuadrada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónRaízCuadradaActionPerformed(evt);
            }
        });

        Botón1.setBackground(new java.awt.Color(51, 51, 51));
        Botón1.setForeground(new java.awt.Color(255, 255, 255));
        Botón1.setText("1");
        Botón1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón1ActionPerformed(evt);
            }
        });

        BotónExponente.setBackground(new java.awt.Color(51, 51, 51));
        BotónExponente.setForeground(new java.awt.Color(255, 255, 255));
        BotónExponente.setText("^");
        BotónExponente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónExponenteActionPerformed(evt);
            }
        });

        Botón4.setBackground(new java.awt.Color(51, 51, 51));
        Botón4.setForeground(new java.awt.Color(255, 255, 255));
        Botón4.setText("4");
        Botón4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón4ActionPerformed(evt);
            }
        });

        BotónParéntesisIzquierdo.setBackground(new java.awt.Color(51, 51, 51));
        BotónParéntesisIzquierdo.setForeground(new java.awt.Color(255, 255, 255));
        BotónParéntesisIzquierdo.setText("(");
        BotónParéntesisIzquierdo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónParéntesisIzquierdoActionPerformed(evt);
            }
        });

        Botón7.setBackground(new java.awt.Color(51, 51, 51));
        Botón7.setForeground(new java.awt.Color(255, 255, 255));
        Botón7.setText("7");
        Botón7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón7ActionPerformed(evt);
            }
        });

        BotónParéntesisDerecho.setBackground(new java.awt.Color(51, 51, 51));
        BotónParéntesisDerecho.setForeground(new java.awt.Color(255, 255, 255));
        BotónParéntesisDerecho.setText(")");
        BotónParéntesisDerecho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónParéntesisDerechoActionPerformed(evt);
            }
        });

        Botón0.setBackground(new java.awt.Color(51, 51, 51));
        Botón0.setForeground(new java.awt.Color(255, 255, 255));
        Botón0.setText("0");
        Botón0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón0ActionPerformed(evt);
            }
        });

        BotónLOG.setBackground(new java.awt.Color(51, 51, 51));
        BotónLOG.setForeground(new java.awt.Color(255, 255, 255));
        BotónLOG.setText("Log");
        BotónLOG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónLOGActionPerformed(evt);
            }
        });

        Botón2.setBackground(new java.awt.Color(51, 51, 51));
        Botón2.setForeground(new java.awt.Color(255, 255, 255));
        Botón2.setText("2");
        Botón2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón2ActionPerformed(evt);
            }
        });

        Botón8.setBackground(new java.awt.Color(51, 51, 51));
        Botón8.setForeground(new java.awt.Color(255, 255, 255));
        Botón8.setText("8");
        Botón8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón8ActionPerformed(evt);
            }
        });

        Botón5.setBackground(new java.awt.Color(51, 51, 51));
        Botón5.setForeground(new java.awt.Color(255, 255, 255));
        Botón5.setText("5");
        Botón5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón5ActionPerformed(evt);
            }
        });

        Botón3.setBackground(new java.awt.Color(51, 51, 51));
        Botón3.setForeground(new java.awt.Color(255, 255, 255));
        Botón3.setText("3");
        Botón3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón3ActionPerformed(evt);
            }
        });

        BotónPunto.setBackground(new java.awt.Color(51, 51, 51));
        BotónPunto.setForeground(new java.awt.Color(255, 255, 255));
        BotónPunto.setText(".");
        BotónPunto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónPuntoActionPerformed(evt);
            }
        });

        BotónDivisión1.setBackground(new java.awt.Color(51, 51, 51));
        BotónDivisión1.setForeground(new java.awt.Color(255, 255, 255));
        BotónDivisión1.setText("a/b");
        BotónDivisión1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónDivisión1ActionPerformed(evt);
            }
        });

        BotónPorcentaje_de_num.setBackground(new java.awt.Color(51, 51, 51));
        BotónPorcentaje_de_num.setForeground(new java.awt.Color(255, 255, 255));
        BotónPorcentaje_de_num.setText("%");
        BotónPorcentaje_de_num.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónPorcentaje_de_numActionPerformed(evt);
            }
        });

        jScrollPane2.setViewportView(Historial);

        jMenu1.setText("Opciones");

        jMenu3.setText("Convertidor");

        jMenuItem2.setText("Volumen");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem2);

        jMenuItem3.setText("Longitud");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem3);

        jMenuItem4.setText("Temperatura");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem4);

        jMenuItem5.setText("Peso");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem5);

        jMenuItem6.setText("Presion");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem6);

        jMenu1.add(jMenu3);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(BotónPorcentaje_de_num, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(BotónDivisión1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(BotónAC, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtpantalla, javax.swing.GroupLayout.PREFERRED_SIZE, 332, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(BotónRaízEnécima, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónRaízCuadrada, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónParéntesisIzquierdo, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónParéntesisDerecho, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónDEL, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(Botón4, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(Botón5, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(Botón6, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(BotónMultiplicar, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(BotónDivisión, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(Botón0, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(BotónPunto, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(BotónPI, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(BotónEuler, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(BotónResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(Botón1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(Botón2, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(Botón3, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(BotónSuma, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(BotónResta, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(Botón7, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(Botón8, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(Botón9, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(BotónLOG, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(BotónExponente, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtpantalla, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 41, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(BotónAC, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BotónDivisión1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BotónPorcentaje_de_num, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BotónRaízEnécima, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónRaízCuadrada, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónParéntesisIzquierdo, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónParéntesisDerecho, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónDEL, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Botón7, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Botón8, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Botón9, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónLOG, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónExponente, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Botón4, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Botón5, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Botón6, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónMultiplicar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónDivisión, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Botón1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Botón2, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Botón3, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónSuma, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónResta, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BotónPunto, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónPI, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónEuler, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Botón0, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35))
        );

        pack();
    }// </editor-fold>                        

    private void Botón9ActionPerformed(java.awt.event.ActionEvent evt) {                                       
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"9");
    }                                      

    private void Botón6ActionPerformed(java.awt.event.ActionEvent evt) {                                       
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"6");
    }                                      

    private void BotónRestaActionPerformed(java.awt.event.ActionEvent evt) {                                           
        // TODO add your handling code here:
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="-";
            txtpantalla.setText("");
        }

    }                                          

    private void BotónDivisiónActionPerformed(java.awt.event.ActionEvent evt) {                                              
        // TODO add your handling code here:
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="/";
            txtpantalla.setText("");
        }
    }                                             

    private void BotónMultiplicarActionPerformed(java.awt.event.ActionEvent evt) {                                                 
        // TODO add your handling code here:
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="*";
            txtpantalla.setText("");
        }
    }                                                

    private void BotónSumaActionPerformed(java.awt.event.ActionEvent evt) {                                          
        // TODO add your handling code here
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="+";
            txtpantalla.setText("");
        }
    }                                         

    private void BotónDELActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
        String cadena;
        cadena=txtpantalla.getText();

        if (cadena.length()>0) {
            cadena=cadena.substring(0, cadena.length()-1);
            txtpantalla.setText(cadena);
        }
    }                                        

    private void BotónResultadoActionPerformed(java.awt.event.ActionEvent evt) {                                               
        // TODO add your handling code here:
        String resultado;
        memoria2=txtpantalla.getText();

        if (!memoria2.equals("")) {
            resultado=calculadora(memoria1,memoria2,signo);
            txtpantalla.setText(resultado);
        }
        agregarH();

    }                                              
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
    
    public static String calculadora(String memoria1,String memoria2,String signo){
    Double resultado=0.0;
    String respuesta;
    
    
    if (signo.equals("-")) {
        
        RESTAR resta = new RESTAR();
        
        resta.setN(memoria1);
        resta.setN2(memoria2);
                    
        resultado = resta.RESTAR_OBTENER(memoria1,memoria2);
        
    }
    if (signo.equals("+")) {
        
        SUMAR suma = new SUMAR();
        
        suma.setN(memoria1);
        suma.setN2(memoria2);
                    
        resultado = suma.SUMAR_OBTENER(memoria1, memoria2);
        
    }
    if (signo.equals("*")) {
        
        MULTIPLICAR multi = new MULTIPLICAR();
        
        multi.setN(memoria1);
        multi.setN2(memoria2);
        
        resultado = multi.MULTIPLICAR_OBTENER(memoria1,memoria2);
                
    }
    if (signo.equals("/")) {
        
        DIVIDIR div = new DIVIDIR();
        
        div.setN(memoria1);
        div.setN2(memoria2);
        
        resultado = div.DIVIDIR_OBTENER(memoria1,memoria2);
                
    }
    
    if (signo.equals("[")) {
        
        RAIZ raiz = new RAIZ();
        
        raiz.setN(memoria1);
                
        resultado = raiz.RAIZ_OBTENER(memoria1);
                
    }
    
    if (signo.equals("^")){
        
        EXPONENTE exponente = new EXPONENTE();
        
        exponente.setN(memoria1);
        exponente.setN2(memoria2);
        
        resultado = exponente.EXPONENTE_OBTENER(memoria1, memoria2);
        
    }
    
    if (signo.equals("°")) { 
        
        RAIZENÉSIMA enésima = new RAIZENÉSIMA();
        
        enésima.setN(memoria1);
        enésima.setN2(memoria2);
                
        resultado = enésima.RAIZENÉSIMA_OBTENER(memoria1, memoria2);
    }
    
    if (signo.equals("%")) { 
        
        PORCENTAJE porcentaje = new PORCENTAJE();
        
        porcentaje.setN(memoria1);
        porcentaje.setN2(memoria2);
                
        resultado = porcentaje.PORCENTAJE_OBTENER(memoria1, memoria2);
    }
    

    
    respuesta=resultado.toString();
    return respuesta;
}
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
    private void BotónACActionPerformed(java.awt.event.ActionEvent evt) {                                        
        // TODO add your handling code here:
        txtpantalla.setText("");
    }                                       

    private void BotónPIActionPerformed(java.awt.event.ActionEvent evt) {                                        
        // TODO add your handling code here:
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="PI";
            txtpantalla.setText("");
        }
    }                                       

    private void BotónEulerActionPerformed(java.awt.event.ActionEvent evt) {                                           
        // TODO add your handling code here:
    }                                          

    private void BotónRaízEnécimaActionPerformed(java.awt.event.ActionEvent evt) {                                                 
        // TODO add your handling code here:
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="°";
            txtpantalla.setText("");
        }
        
    }                                                

    private void BotónRaízCuadradaActionPerformed(java.awt.event.ActionEvent evt) {                                                  
        // TODO add your handling code here:
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="[";
            txtpantalla.setText("");
        }
    }                                                 

    private void Botón1ActionPerformed(java.awt.event.ActionEvent evt) {                                       
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"1");
    }                                      

    private void BotónExponenteActionPerformed(java.awt.event.ActionEvent evt) {                                               
        // TODO add your handling code here:
         if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="^";
            txtpantalla.setText("");
        }
    }                                              

    private void Botón4ActionPerformed(java.awt.event.ActionEvent evt) {                                       
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"4");
    }                                      

    private void BotónParéntesisIzquierdoActionPerformed(java.awt.event.ActionEvent evt) {                                                         
        // TODO add your handling code here:
    }                                                        

    private void Botón7ActionPerformed(java.awt.event.ActionEvent evt) {                                       
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"7");
    }                                      

    private void BotónParéntesisDerechoActionPerformed(java.awt.event.ActionEvent evt) {                                                       
        // TODO add your handling code here:
    }                                                      

    private void Botón0ActionPerformed(java.awt.event.ActionEvent evt) {                                       
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"0");
    }                                      

    private void BotónLOGActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
    }                                        

    private void Botón2ActionPerformed(java.awt.event.ActionEvent evt) {                                       
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"2");
    }                                      

    private void Botón8ActionPerformed(java.awt.event.ActionEvent evt) {                                       
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"8");
    }                                      

    private void Botón5ActionPerformed(java.awt.event.ActionEvent evt) {                                       
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"5");
    }                                      

    private void Botón3ActionPerformed(java.awt.event.ActionEvent evt) {                                       
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"3");
    }                                      

    private void BotónPuntoActionPerformed(java.awt.event.ActionEvent evt) {                                           
        // TODO add your handling code here:
        String cadena;
        cadena=txtpantalla.getText();

        if (cadena.length()<=0) {
            txtpantalla.setText("0.");

        }
        else{
            if (!existepunto(txtpantalla.getText())) {
                txtpantalla.setText(txtpantalla.getText()+".");

            }
        }

    }                                          

    private void BotónDivisión1ActionPerformed(java.awt.event.ActionEvent evt) {                                               
        // TODO add your handling code here:
    }                                              

    private void BotónPorcentaje_de_numActionPerformed(java.awt.event.ActionEvent evt) {                                                       
        // TODO add your handling code here:
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="%";
            txtpantalla.setText("");
        }
        
    }                                                      

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {                                           
              new Volumen().setVisible(true);
    }                                          

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {                                           
        new Longitud().setVisible(true);
    }                                          

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {                                           
        new Temperatura().setVisible(true);
    }                                          

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {                                           
        new Peso().setVisible(true);
    }                                          

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {                                           
        new Presion().setVisible(true);
    }                                          

    private void txtpantallaActionPerformed(java.awt.event.ActionEvent evt) {                                            
        // TODO add your handling code here:
    }                                           

    public static boolean existepunto(String cadena){
        boolean resultado;
        resultado=false;
        
        for (int i = 0; i < cadena.length(); i++) {
            if (cadena.substring(i, i+1).equals(".")) {
                resultado=true;
                break;
                
                
                
                
            }
            
        }
        return resultado;
        
                
    }
    
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmCALCULADORA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmCALCULADORA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmCALCULADORA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmCALCULADORA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(() -> {
            new FrmCALCULADORA().setVisible(true);
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton Botón0;
    private javax.swing.JButton Botón1;
    private javax.swing.JButton Botón2;
    private javax.swing.JButton Botón3;
    private javax.swing.JButton Botón4;
    private javax.swing.JButton Botón5;
    private javax.swing.JButton Botón6;
    private javax.swing.JButton Botón7;
    private javax.swing.JButton Botón8;
    private javax.swing.JButton Botón9;
    private javax.swing.JButton BotónAC;
    private javax.swing.JButton BotónDEL;
    private javax.swing.JButton BotónDivisión;
    private javax.swing.JButton BotónDivisión1;
    private javax.swing.JButton BotónEuler;
    private javax.swing.JButton BotónExponente;
    private javax.swing.JButton BotónLOG;
    private javax.swing.JButton BotónMultiplicar;
    private javax.swing.JButton BotónPI;
    private javax.swing.JButton BotónParéntesisDerecho;
    private javax.swing.JButton BotónParéntesisIzquierdo;
    private javax.swing.JButton BotónPorcentaje_de_num;
    private javax.swing.JButton BotónPunto;
    private javax.swing.JButton BotónRaízCuadrada;
    private javax.swing.JButton BotónRaízEnécima;
    private javax.swing.JButton BotónResta;
    private javax.swing.JButton BotónResultado;
    private javax.swing.JButton BotónSuma;
    private javax.swing.JList Historial;
    private javax.swing.JList jList1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTextField txtpantalla;
    // End of variables declaration                   
}
