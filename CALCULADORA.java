import java.util.InputMismatchException;
import java.util.Scanner;


public class CALCULADORA {

   
    public static void main(String[] args) {
        
        Scanner lectura = new Scanner(System.in);
        
        boolean salir=false;
        int opcion;
        String n;
        String n2;
        
       
        while(!salir){
            System.out.println("##### Bienvenido #####");
            System.out.println("Qué deseas hacer: ");
            System.out.println("\n1) Sumar.");
            System.out.println("2) Restar.");
            System.out.println("3) Multiplicar.");
            System.out.println("4) Dividir.");
            System.out.println("5) Salir.");
            
            
            try{
            
            System.out.println("\nEscoje una de las opciones: ");
            opcion = lectura.nextInt();
            
            switch(opcion){
                
                case 1:
                    
                    SUMAR suma = new SUMAR();
                    
                    System.out.println("Ingrese número 1: ");
                    n=lectura.nextLine();
                    System.out.println("Ingrese número 2: ");
                    n2=lectura.nextLine();
                    
                    suma.setN(n);
                    suma.setN2(n2);
                    
                    suma.SUMAR_OBTENER(n,n2);
                    
                    break;
                    
                case 2:
                    
                    RESTAR resta = new RESTAR();
                    
                    System.out.println("Ingrese número 1: ");
                    n=lectura.nextLine();
                    System.out.println("Ingrese número 2: ");
                    n2=lectura.nextLine();
                    
                    resta.setN(n);
                    resta.setN2(n2);
                    
                    resta.RESTAR_OBTENER(n,n2);
                    
                    break;
                    
                case 3:
                    
                    MULTIPLICAR mult = new MULTIPLICAR();
                    
                    System.out.println("Ingrese número 1: ");
                    n=lectura.nextLine();
                    System.out.println("Ingrese número 2: ");
                    n2=lectura.nextLine();
                    
                    mult.setN(n);
                    mult.setN2(n2);
                    
                    mult.MULTIPLICAR_OBTENER(n,n2);
                    
                    break;
                    
                case 4:
                    
                    DIVIDIR div = new DIVIDIR();
                    
                    System.out.println("Ingrese número 1: ");
                    n=lectura.nextLine();
                    System.out.println("Ingrese número 2: ");
                    n2=lectura.nextLine();
                    
                    div.setN(n);
                    div.setN2(n2);
                    
                    div.DIVIDIR_OBTENER(n,n2);
                    
                    break;
                    
                case 5:
                    
                    salir=true;
                    
                    break;
                
                default:
                    System.out.println("\n\nopción invalida!\n\n");
            }
        
        }catch(InputMismatchException e){
                System.out.println("\nDebes ingresar un número.\nf");
                lectura.next();
        }
        }
        System.out.println("\n##### Hasta luego #####\n\n");
    }
    
}
