import java.util.Scanner;

/**
 *
 * @author Mateus
 */
public class RAIZENÉSIMA{
    Scanner lectura = new Scanner(System.in);

    String n;
    String n2;

    public RAIZENÉSIMA(String n, String n2){
        this.n=n;
        this.n2=n2;
    }

    RAIZENÉSIMA(){

    }

    public String getN(){
        return n;
    }

    public String getN2(){
        return n2;
    }

    public void setN(String n){
        this.n=n;
    }

    public void setN2(String n2){
        this.n2=n2;
    }

    public double RAIZENÉSIMA_OBTENER(String n, String n2){
        double resultado, c;
        c = 1/Double.parseDouble(n);
        resultado = (Double)Math.pow(Double.parseDouble(n2), c);
        return resultado;
    }

}
